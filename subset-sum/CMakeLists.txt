cmake_minimum_required(VERSION 2.8)
project(subset-sum)

include(../common.cmake)

set(SRCS find_subsets.cpp)

if (TEST_SOLUTION)
    set(SRCS ../private/subset-sum/threads.cpp)
endif()

if (ENABLE_PRIVATE_TESTS)
endif()

add_library(find_subsets ${SRCS})

add_gtest(test_solution test.cpp)
target_link_libraries(test_solution find_subsets)

add_benchmark(bench_solution run.cpp)
target_link_libraries(bench_solution find_subsets)
