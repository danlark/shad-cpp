#pragma once

#include <atomic>

struct RWSpinLock {
    void lockRead() {
    }

    void unlockRead() {
    }

    void lockWrite() {
    }

    void unlockWrite() {
    }
};
