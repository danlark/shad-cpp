cmake_minimum_required(VERSION 2.8)
project(mpmc-bounded)

if (TEST_SOLUTION)
  include_directories(../private/fast-queue)
endif()

include(../common.cmake)

add_gtest(test_mpmc_bounded test.cpp)

add_benchmark(bench_mpmc_bounded run.cpp)
